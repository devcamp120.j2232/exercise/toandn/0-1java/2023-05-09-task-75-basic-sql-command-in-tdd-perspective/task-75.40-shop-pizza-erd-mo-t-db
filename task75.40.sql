--1 Hiển thị toàn bộ thông tin khách hàng (customers) có credit > 50000
SELECT * FROM customers AS c WHERE c.credit_limit > 50000;

--2 Hiển thị toàn bộ thông tin danh sách sản phẩm, với tên của product_line tương ứng (không hiển thị product_line_id mà hiển thị tên product line)
SELECT p.id, p.product_code, p.product_name, p.product_description, p.product_line_id, p.product_scale, p.product_vendor, p.quantity_in_stock, p.buy_price, pl.product_line FROM products AS p INNER JOIN product_lines AS pl ON p.product_line_id = pl.id;

--3 Đếm xem ở từng nước, có bao nhiêu khách hàng? (hiển thị tên nước, số lượng khách hàng)
SELECT c.country, COUNT(*) AS so_luong_khach_hang FROM customers AS c GROUP BY c.country;

--4 Sắp xếp các sản phẩm theo giá mua tăng dần và lấy ra 10 sản phẩm đầu tiên
SELECT * FROM products AS p ORDER BY p.buy_price ASC LIMIT 10;

--5 Lấy ra danh sách các đơn hàng được đặt vào tháng 10/2003
SELECT * FROM orders As o WHERE o.order_date >= '2003-10-01' && o.order_date <= '2003-10-31';

--6 Lấy ra danh sách các khách hàng có số lượng đơn hàng > 50
SELECT CONCAT(c.first_name, " ", c.last_name) AS fullname, od.quantity_order FROM customers AS c INNER JOIN orders AS o ON c.id = o.customer_id INNER JOIN order_details AS od ON o.id = od.order_id WHERE od.quantity_order > 50;

--7 Tính toán xem, mỗi đơn hàng có bao nhiêu sản phẩm được đặt và tổng tiền từng đơn hàng là bao nhiêu Hiển thị: order_id, tổng số lượng và tổng tiền
SELECT od.order_id, od.quantity_order, SUM(od.price_each * od.quantity_order) AS total_price FROM order_details AS od GROUP BY od.order_id;

--8 Tính toán xem, mỗi đơn hàng có bao nhiêu sản phẩm được đặt và tổng tiền từng đơn hàng là bao nhiêu. Và lấy ra danh sách các đơn có tổng tiền > 100000 Hiển thị: order_id, tổng số lượng và tổng tiền

--9 Tìm và hiển thị toàn bộ thông tin sản phẩm được đặt nhiều nhất (theo quantity_order) trong năm 2004











